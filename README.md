Definition of the base runner images for Felix CI.
These images are used for Runners on GitLab-CI.
SLC6 and CC7 use different Dockerfiles.

To see what to install in the docket, log in to ```vm-atlas-felix-ci-0x``` 
and run
```
docker run -it --net=host gitlab-registry.cern.ch/atlas-tdaq-felix/gitlab_ci_runner:mkdocs /bin/bash
```
